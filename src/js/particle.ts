const colors = ['red', 'blue', 'green', 'purple', 'yellow'];
const particleClass = 'particle';

var generateParticle = function () {
    let el = document.createElement('div');
    el.classList.add(particleClass, randomColor());

    let pos = randomPosition();

    el.style.top = pos.y + 'vh';
    el.style.left = pos.x + 'vw';

    el.style.transform = 'rotate(' + rand(270, 90) + 'deg)';

    document.querySelector('#particle-container').appendChild(el);
};

var randomColor = function () {
    let index = rand(colors.length);
    return colors[index];
};

var randomPosition = function () {
    let x = rand(100);
    let y = rand(100);

    return {x: x, y: y};
};

var rand = function (upperBound: number, lowerBound?: number) {
    upperBound = +upperBound || 0;
    lowerBound = +lowerBound || 0;

    return Math.floor(Math.random() * 100 % upperBound) + lowerBound;
};

export {
    generateParticle,
    rand
};
