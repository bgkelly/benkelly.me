import './index.css';
//import './index.html';
//import './resume/index.html';
import {generateParticle, rand} from './js/particle';

var init = function () {
    let density = 100;
    let lastTimeout = 0;

    for (let i = 0; i < density; i++) {
        let timeout = rand(5000, lastTimeout);
        window.setTimeout(generateParticle, lastTimeout + timeout);
        lastTimeout = timeout;
    }
};

init();
