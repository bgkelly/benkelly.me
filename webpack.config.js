const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const svgToMiniDataURI = require("mini-svg-data-uri");

module.exports = (_, argv) => {
    var isProd = argv.mode === "production";
    return {
        entry: {
            index: "./src/index.ts",
        },
        mode: isProd ? "production" : "development",
        devtool: isProd ? false : "inline-source-map",
        devServer: {
            open: true,
            static: path.join(__dirname, "./dist"),
            compress: true,
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: "ts-loader",
                    exclude: /node_modules/,
                },
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, "css-loader"],
                },
                {
                    test: /\.svg$/i,
                    type: "asset/inline",
                    generator: {
                        dataUrl: (content) =>
                            svgToMiniDataURI(content.toString()),
                    },
                },
            ],
        },
        resolve: {
            extensions: [".tsx", ".ts", ".js"],
        },
        output: {
            filename: "[name].bundle.js",
            path: path.resolve(__dirname, "dist"),
        },
        plugins: [
            new MiniCssExtractPlugin(),
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                template: "./src/index.html",
                filename: "index.html"
            }),
            new HtmlWebpackPlugin({
                template: "./src/resume.html",
                filename: "resume/index.html"
            }),
        ],
    };
};